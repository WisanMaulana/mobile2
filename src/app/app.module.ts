import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, Tabs, Modal } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LibraryPage } from '../pages/library/library';
import { FavoritPage } from '../pages/favorit/favorit';
import { TabsPage } from '../pages/tabs/tabs';
import { QuotesPage } from '../pages/quotes/quotes';
import { ModalFavoritePage } from '../pages/modal-favorite/modal-favorite';
import { QuotesService } from '../services/quotes';

import { SettingsService } from '../services/settings';
import { SettingsPage } from '../pages/settings/settings';
import { SkripsiPage } from '../pages/skripsi/skripsi';
import { ModalSkripsiPage } from '../pages/modal-skripsi/modal-skripsi';




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LibraryPage,
    FavoritPage,
    TabsPage,
    QuotesPage,
    ModalFavoritePage,
    SkripsiPage,
    ModalSkripsiPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LibraryPage,
    FavoritPage,
    TabsPage,
    QuotesPage,
    ModalFavoritePage,
    SkripsiPage,
    ModalSkripsiPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    QuotesService,
    SettingsService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
