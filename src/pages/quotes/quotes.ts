import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {Quote} from '../../data/quote.interface';
import { QuotesService } from '../../services/quotes';


/**
 * Generated class for the QuotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage implements OnInit {
  quotes: {category: string, quotes:Quote[], icon:string};
  
  constructor(private navParams: NavParams,private alertCtrl: AlertController, private quoteSrv: QuotesService) {

  }
  unFavorite(quote:Quote){
    return this.quoteSrv.isFavorite(quote);
   }

  onShowAlert (data) {
    const alert = this.alertCtrl.create({
    title: 'Add Quote',
    message: 'Are you sure you want to add this to Favourite?',
    buttons: [
    {
    text: 'OK',
    handler: () => {
      console.log(data);
      this.quoteSrv.addQuoteToFavorites(data);
      
        let a = this.quoteSrv.getAllFavoriteQuotes(data);
        console.log(a)
        
      }
      },
      {
      text: 'Cancel',
      role: 'cancel',
      handler: () => {
      console.log("Cancel is clicked.");
      }
      }
      ]
      });
      alert.present();
      }

      onDelete(quote:Quote){
        this.quoteSrv.removeQuoteFromFavorites(quote);
      }



      deleteFav(item) {
        const alert = this.alertCtrl.create({
        title: 'Delete Quote',
        message: 'Are you sure you want to delete this Favourite?',
        buttons: [
        {
        text: 'OK',
        handler: () => {
          
          
            this.quoteSrv.removeQuoteFromFavorites(item);
            console.log(this.quoteSrv);
            
          }
          },
          {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          console.log("Cancel is clicked.");
          }
          }
          ]
          });
          alert.present();
          }

  ngOnInit(){
    this.quotes = this.navParams.data;
    console.log(this.quotes);
  }

}
