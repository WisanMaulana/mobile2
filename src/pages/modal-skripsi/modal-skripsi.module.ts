import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalSkripsiPage } from './modal-skripsi';

@NgModule({
  declarations: [
    ModalSkripsiPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalSkripsiPage),
  ],
})
export class ModalSkripsiPageModule {}
