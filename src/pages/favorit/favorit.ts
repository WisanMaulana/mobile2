import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Item, ModalController, AlertController } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import { ModalFavoritePage } from '../modal-favorite/modal-favorite';
import { SettingsService } from '../../services/settings';

/**
 * Generated class for the FavoritPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorit',
  templateUrl: 'favorit.html',
})
export class FavoritPage implements OnInit {

  favoriteCollection : any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private quoteService: QuotesService, private modalCtrl:ModalController,
  public alertCtrl: AlertController,
  private settingsService: SettingsService) {
      this.favoriteCollection=this.quoteService.getAllFavorite();
  }

  showAlert(item){
    console.log(item)
    let modal = this.modalCtrl.create(ModalFavoritePage,{data:item});
    modal.onDidDismiss((data) => this.favoriteCollection = data);
    modal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritPage');
    this.favoriteCollection=this.quoteService.getAllFavorite();
  }

  ngOnInit(){
    this.favoriteCollection=this.quoteService.getAllFavorite();
  }

  ionViewWillEnter() {
    this.favoriteCollection=this.quoteService.getAllFavorite(); 
  }

  deleteFav(item){
    const alert = this.alertCtrl.create({
      title: 'Delete Quote',
      message: 'Are you sure you want to delete this Favourite?',
      buttons: [
      {
      text: 'OK',
      handler: () => {
        
        
          this.quoteService.removeQuoteFromFavorites(item);
          console.log(this.quoteService.getAllFavorite());
          
          
        }
        },
        {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
        console.log("Cancel is clicked.");
        }
        }
        ]
        });
        alert.present();

  }

  setBgColor(){
    return this.settingsService.isAltBackground() ?'secondary' : 'altQuoteBg'  ;
  }

}
