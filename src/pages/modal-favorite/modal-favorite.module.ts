import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalFavoritePage } from './modal-favorite';

@NgModule({
  declarations: [
    ModalFavoritePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFavoritePage),
  ],
})
export class ModalFavoritePageModule {}
