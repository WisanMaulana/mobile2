import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import {Quote} from '../../data/quote.interface';
import { FavoritPage } from '../favorit/favorit';

/**
 * Generated class for the ModalFavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-favorite',
  templateUrl: 'modal-favorite.html',
})
export class ModalFavoritePage {
  favoriteCollection : any[];
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, private alertCtrl: AlertController, 
    private quoteSrv: QuotesService,
    public viewCtrl: ViewController) {
    this.favoriteCollection = [this.navParams.get('data')];
    console.log(this.favoriteCollection);
  }

  closeModal(){
    this.viewCtrl.dismiss(this.favoriteCollection);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalFavoritePage');
  }

  ionViewWillEnter() {
    
  }

  unFavorite(quote:Quote){
  return this.quoteSrv.isFavorite(quote);
  }

  deleteFav(item) {
    const alert = this.alertCtrl.create({
    title: 'Delete Quote',
    message: 'Are you sure you want to delete this Favourite?',
    buttons: [
    {
    text: 'OK',
    handler: () => {
      
      
        this.quoteSrv.removeQuoteFromFavorites(item);
        console.log(this.quoteSrv.getAllFavorite());
        
        
      }
      },
      {
      text: 'Cancel',
      role: 'cancel',
      handler: () => {
      console.log("Cancel is clicked.");
      }
      }
      ]
      });
      alert.present();
      }





}
