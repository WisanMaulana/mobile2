import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LibraryPage } from '../library/library';
import { FavoritPage } from '../favorit/favorit';

@IonicPage()
@Component({
  selector: 'page-tabs',
  template:`
    <ion-tabs>
      <ion-tab [root]='libraryPage' tabTitle='Library' tabIcon='book'></ion-tab>
      <ion-tab [root]='favoritPage' tabTitle='Favorit' tabIcon='star'></ion-tab>
    </ion-tabs>
  `
})
export class TabsPage {

  libraryPage = LibraryPage;
  favoritPage = FavoritPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
