import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import {
  FormGroup,
  FormControl,
  Validators,
  FormArrayName,
  FormArray
} from "@angular/forms";
import { ModalSkripsiPage } from "../modal-skripsi/modal-skripsi";

/**
 * Generated class for the SkripsiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-skripsi",
  templateUrl: "skripsi.html"
})
export class SkripsiPage implements OnInit {

  data;
  input:{
    label:string,
    value:string,
    formCtrl:any
  }={
    label:'pembimbing',
    value:"",
    formCtrl:null
  }

  
  
  

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController
  ) {
    this.data = new Array();
    this.input.formCtrl =   [new FormControl(" ", Validators.required)];
    this.data.push(this.input);


  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad SkripsiPage');
  // }

  skrispsiForm: FormGroup;
  ngOnInit() {
    this.initializeForm();
  }
  private initializeForm() {
    this.skrispsiForm = new FormGroup({
      nama: new FormControl(null, Validators.required),
      nim: new FormControl(null, Validators.required),
      classOf: new FormControl("2014", Validators.required),
      pembimbingStatus: new FormArray(this.input.formCtrl),
      lintasProdi: new FormControl(false)
    });
  }
  onSubmit() {
    console.log(this.skrispsiForm.value);

    let modal = this.modalCtrl.create(ModalSkripsiPage, {
      data: this.skrispsiForm.value
    });

    modal.present();
  }

  add(){
    this.input.formCtrl.push(new FormControl(" ", Validators.required));
    console.log(this.input);

    this.data.push(this.input)
  }
}
